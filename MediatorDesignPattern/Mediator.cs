﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorDesignPattern
{
    class Mediator : IMediator // Se crea la clase mediador e implementamos la interfaz IMediator
    {
        private List<IColleague> colleagues; // Creamos una lista de la clase abstracta creada

        public Mediator() // Creamos nuestro constructor para que inicialice a los objetos colega
        {
            colleagues = new List<IColleague>();
        }

        public void Add(IColleague colleague) // Creamos el metodo add para que comunique con la clase abstracta
        {
            this.colleagues.Add(colleague);
        }

        public void Send(string message, IColleague colleague) // Implementacion de la interfaz IMediator
        {
            foreach (var c in this.colleagues) // Se agrega un foreach para poder hacer un recorrido a todos los colegas add
            {
                if (colleague != c) // Establecemos la condicion que el colega sea diferente del que se va agregando, entonces recibe el mensaje.
                {
                    c.Receive(message); // Recibe el mensaje
                }
            }
        }
    }
}
