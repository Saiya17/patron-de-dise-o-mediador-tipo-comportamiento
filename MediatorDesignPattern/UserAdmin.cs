﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorDesignPattern
{
    public class UserAdmin : IColleague // Se crea la clase UserAdmin que implementa la interfaz o clase abstracta Icollega
    {
        public UserAdmin(IMediator mediator) : base(mediator) // Recibe a nuestro mediador y lo mandamos a base (De esta manera se invoca al contructor que estamos heredando)
        {

        }

        public override void Receive(string message) // Metodo llamado de la Interfaz Icollega
        {
            Console.WriteLine("Un administrador recibe: "+message);
            Console.WriteLine("Se notifico por mail");
        }
    }
}
