﻿using System;

namespace MediatorDesignPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Mediator mediador = new Mediator(); //Creamos el mediador

            IColleague oPepe = new User(mediador); // Colega 1
            IColleague oAdmin = new UserAdmin(mediador); // Colega 2
            IColleague oAdmin2 = new UserAdmin(mediador); // Colega 3

            mediador.Add(oPepe);  // Llamamos a nuestro mediador y el metodo add que recibe algun objeto que ahiga implementado la interfaz colega
            mediador.Add(oAdmin);  // Llamamos a nuestro mediador y el metodo add que recibe algun objeto que ahiga implementado la interfaz colega
            mediador.Add(oAdmin2);  // Llamamos a nuestro mediador y el metodo add que recibe algun objeto que ahiga implementado la interfaz colega

            oPepe.Communicate("Oye admin tengo un problema"); // Mensaje de Pepe al Admin
        }
    }
}
