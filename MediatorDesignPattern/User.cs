﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorDesignPattern
{
    class User : IColleague // Se crea la clase User que implementa la interfaz o clase abstracta Icollega
    {
        public User(IMediator mediator) : base(mediator) // Recibe a nuestro mediador y lo mandamos a base (De esta manera se invoca al contructor que estamos heredando)
        {

        }

        public override void Receive(string message) // Metodo llamado de la Interfaz Icollega
        {
            Console.WriteLine("Un usuario recibe: " + message);
        }
    }
}
