﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorDesignPattern
{
    public abstract class IColleague // Se crea una clase de tipo abstracta llamada Colega
    {
        private IMediator mediator; // Llamamos a nuestro mediador

        public IMediator Mediator // Creamos una propiedad publica para nuestra interfaz mediador
        {
            get;
        }

        public IColleague(IMediator mediator) // Hacemos que el mediador sea recibido en el constructor
        {
            this.mediator = mediator;
        }

        public void Communicate(string message) // Creamos el metodo comunicate que permite hacer el envio del mensaje a traves del mediador
        {
            this.mediator.Send(message,this);
        }

        public abstract void Receive(string message); // Metodo abstracto que recibe el mensaje
    }
}
