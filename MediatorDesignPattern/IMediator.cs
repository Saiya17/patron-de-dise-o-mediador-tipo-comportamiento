﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediatorDesignPattern
{
    public interface IMediator // Se agrega una interfaz llamada mediador de tipo publica
    {
        void Send(string message, IColleague colleague);
        // Ejemplo de simulación de un chat para explicar el patrón de diseño mediator, creamos el metodo send.
        // El método send contiene un mensaje y que es enviado a un colega.
    }
}
